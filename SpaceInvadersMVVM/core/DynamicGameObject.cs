using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpaceInvadersMVVM.GameEntites;

namespace SpaceInvadersMVVM.core;
public class DynamicGameObject : GameObject
{
    private int _speed;
    private bool _isColliding;
    public DynamicGameObject(Canvas gameScreen) : base(gameScreen) { }

    public event EventHandler CollisionDetected;

    public bool IsColliding { get { return _isColliding; } set { SetProperty(ref _isColliding, value, nameof(IsColliding)); } }
    public int Speed { get { return _speed; } set { SetProperty(ref _speed, value, nameof(Speed)); } }

    /* direccion es un vector unitario que indica la direccion del movimeinto
     * (1, 0) -> , (0,1) arriva, (-1, 0 ) <-, (0,-1) abajo,
     * (1,1) diagonal I cuandrante, (-1,1) IIcuadrante, (1,-1) III, (-1,-1) IV */
    public virtual void Move()
    {
        this.Position = CalcMovement(Direction);
    }

    private Vector2D CalcMovement(Vector2D direction) {
        return (direction * Speed) + Position;
    }

    public static bool CheckObjectCollision(GameObject obj1, GameObject obj2)
    {
        Rectangle rect1 = new Rectangle(obj1.Position.X, obj1.Position.Y, obj1.RectWidth, obj1.RectHeight);
        Rectangle rect2 = new Rectangle(obj2.Position.X, obj2.Position.Y, obj2.RectWidth, obj2.RectHeight);

        return rect1.IntersectsWith(rect2);
      
    }

    public bool CheckCanvasCollision()
    {
        if (CalcMovement(Direction).X > this.GameScreen.Width - RectWidth || CalcMovement(Direction).X < 0 || CalcMovement(Direction).Y > this.GameScreen.Height - RectHeight || CalcMovement(Direction).Y < 0)
        {
            IsColliding = true;
            OnCollisionDetected();
            return true;
        }
        else {
            IsColliding = false;
            return false;
        }

    }

    // Método para notificar a los suscriptores cuando se detecta una colisión
    public virtual void OnCollisionDetected()
    {
      
       CollisionDetected?.Invoke(this, EventArgs.Empty);
     
    }
    

}

