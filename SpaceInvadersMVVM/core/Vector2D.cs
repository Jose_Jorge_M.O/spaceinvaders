using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceInvadersMVVM.core;
public class Vector2D : ObservableObject
{
    private int _x;
    private int _y;
    public int X { get { return _x; } set { SetProperty(ref _x, value, nameof(X)); } }
    public int Y { get {return _y; } set {SetProperty(ref _y, value, nameof(Y)); } }

    public Vector2D(int x, int y)
    {
        X = x;
        Y = y;
    }
    public static Vector2D operator +(Vector2D v1, Vector2D v2)
    {
        return new Vector2D(v1.X + v2.X, v1.Y + v2.Y);
    }
    public void Normalize()
    {
        int length = (int) Math.Sqrt(X * X + Y * Y);

        if (length != 0)
        {
            X /= length;
            Y /= length;
        }
    }
    public static Vector2D operator *(Vector2D v1, int scalar)
    {
        return new Vector2D(v1.X * scalar, v1.Y * scalar);
    }

    public override string ToString()
    {
        return $"({X},{Y})";
    }
}
