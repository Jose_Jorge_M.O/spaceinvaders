namespace SpaceInvadersMVVM.core;

public enum Directions
{
    LEFT,
    RIGHT,
    UP, 
    DOWN,
    NULL,
}

public static class DirectionExtensions
{
    public static Vector2D GetVector(this Directions direction)
    {
        switch (direction)
        {
            case Directions.LEFT:
                return new Vector2D(-1, 0);
            case Directions.RIGHT:
                return new Vector2D(1, 0);
            case Directions.DOWN:
                return new Vector2D(0, 1);
            case Directions.NULL:
                return new Vector2D(0, 0);
            case Directions.UP:
                return new Vector2D(0, -1);
            default:
                throw new ArgumentOutOfRangeException(nameof(direction), direction, null);
        }
    }
}

