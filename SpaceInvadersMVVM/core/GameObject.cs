using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.UI.Xaml.Media.Imaging;

namespace SpaceInvadersMVVM.core;
public class GameObject : ObservableObject
{
    private int _rectWidth;
    private int _rectHeight;
    private Vector2D _position;
    private int _hp;
    private string _source;
    private BitmapImage _asset;
    private Canvas _gameScreen;
    private Vector2D _direction;
    public int id;
    public int RectWidth {
        get { return _rectWidth; }
        set { SetProperty(ref _rectWidth, value, nameof(RectWidth)); }
    }
    public int RectHeight {
        get { return _rectHeight; }
        set { SetProperty(ref _rectHeight, value, nameof(RectWidth)); } 
    }
    public Vector2D Position {
        get { return _position; }
        set { SetProperty(ref _position, value, nameof(Position)); }
    }
    public int HP {
        get { return _hp; }
        set { SetProperty(ref _hp, value, nameof(HP)); }
    }
    public string Source {
        get { return _source; }
        set { SetProperty(ref _source, value, nameof(Source)); }
    }
    public BitmapImage Asset {
        get { return _asset; }
        set { SetProperty(ref _asset, value, nameof(Asset)); }
    }

    public Canvas GameScreen {
        get { return _gameScreen; }
        set { SetProperty(ref _gameScreen, value, nameof(GameScreen)); }
    }

    public Vector2D Direction {
        get { return _direction; }
        set { SetProperty(ref _direction, value, nameof(Direction)); }
    }

    public GameObject(Canvas gameScreen) {
        if (gameScreen == null) {
            throw new ArgumentNullException(nameof(gameScreen));
        }
        GameScreen = gameScreen;
    }

    public virtual void Update() { 
       
    }

    public virtual void Hit() { }
}
