using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.UI.Xaml.Controls;
using SpaceInvadersMVVM.core;
using SpaceInvadersMVVM.GameEntites;

namespace SpaceInvadersMVVM.ViewModel;
public class GameViewModel : ViewModelBase
{
    private DispatcherTimer gameLoopTimer = new DispatcherTimer();
    private ObservableCollection<GameObject> _gameEntities;
    private Canvas _gameScreen;
    private EnemyMarix enemyMarix;
    private Directions _inputdirection;
    private int _score;
    private int _canvasLeft;
    private bool _shoot;
    private bool _shooting;
    private string _soundSource;
    private MediaPlayerElement _laser;
    private MediaPlayerElement _explosion;

    public MediaPlayerElement Explosion
    {
        get { return _explosion; }
        set { SetProperty(ref _explosion, value, nameof(Explosion)); }
    }

    public MediaPlayerElement Laser {
        get {  return _laser; }
        set { SetProperty(ref _laser, value, nameof(Laser)); }
    }
    public string SoundSource {
        get { return _soundSource; }
        set { SetProperty(ref _soundSource, value, nameof(SoundSource)); }
    }
    public bool Shooting { get { return _shooting; } set { SetProperty(ref _shooting, value, nameof(Shooting)); } }
    public bool Shoot { get {return _shoot; } set {SetProperty(ref _shoot, value, nameof(Shoot)); } }
    public Directions Inputdirection {
        get { return _inputdirection; }
        set { SetProperty(ref _inputdirection, value, nameof(Inputdirection)); }
    }
    public Canvas GameScreen { get { return _gameScreen; } set { SetProperty(ref _gameScreen, value, nameof(GameScreen)); } }

    public ObservableCollection<GameObject> GameEntities {
        get { return _gameEntities; }
        set { SetProperty(ref _gameEntities, value, nameof(GameEntites)); }
    }

    public int Score {
        get { return _score; }
        set { SetProperty(ref _score, value, nameof(Score)); }
    }

    public int CanvasLeft
    {
        get { return _canvasLeft; }
        set { SetProperty(ref _canvasLeft, value, nameof(CanvasLeft)); }
    }

    public void InpuShoot() {
        if (Shooting == false) {
            Shoot = true;
            
        }
    }
    public GameViewModel(Canvas gameScreen)
    {
        GameEntities = new ObservableCollection<GameObject>() { };
        GameScreen = gameScreen;
        SoundSource = "ms-appx:///SpaceInvadersMVVM/Assets/spaceInvader/laser.mp3";
        GenerateEnemies();
        GenerateDefences();
        GeneratePlayer();
        gameLoopTimer.Tick += GameLoopTimer_Tick;
        gameLoopTimer.Interval = TimeSpan.FromMilliseconds(100);
        gameLoopTimer.Start();
    }

    private void MoveDownMatrix(object sender, EventArgs e)
    {

        foreach (DynamicGameObject entity in GameEntities) {
            if (entity is Enemy)
            {
                entity.Position.Y += 20;

                if (entity.Direction.X == -1)
                {
                    entity.Direction = Directions.RIGHT.GetVector();
                    entity.Position.X += 1;
                }
                else {
                    entity.Direction = Directions.LEFT.GetVector();
                    entity.Position.X -= 1;
                }
                
            }
        }
    }

    private void GameLoopTimer_Tick(object sender, object e)
    {
        SoundSource = "ms-appx:///SpaceInvadersMVVM/Assets/spaceInvader/laser.mp3";

        GameScreen.Focus(FocusState.Programmatic);
        for (int i = 0; i < GameEntities.Count; i++) {
           

            var obj1 = GameEntities[i];

            for (int j = i + 1; j < GameEntities.Count; j++)
            {
                var obj2 = GameEntities[j];

                if (DynamicGameObject.CheckObjectCollision(obj1, obj2))
                {
                    
                    if (obj1 is Bullet && !(obj2 is Player) && !(obj2 is Defence)) {
                        Explosion.MediaPlayer.Play();
                        GameEntities.Remove(obj2);
                        GameEntities.Remove(obj1);
                        Score = Score + 40;
                        Shooting = false;
                    } if (obj2 is Bullet && !(obj1 is Player) && !(obj1 is Defence))
                    {
                        Score = Score + 40;
                        Explosion.MediaPlayer.Play();
                        Shooting = false;
                        GameEntities.Remove(obj2);
                        GameEntities.Remove(obj1);
                    }
                    if (obj1 is Bullet && (obj2 is Defence))
                    {
                        if (obj2.HP == 0)
                        {
                            GameEntities.Remove(obj2);
                            GameEntities.Remove(obj1);
                           
                        }
                        else {
                            obj2.Hit();
                            GameEntities.Remove(obj1);
                           
                        }
                        Shooting = false;


                    }
                    if (obj2 is Bullet && (obj1 is Defence))
                    {
                        if (obj1.HP == 0)
                        {
                            GameEntities.Remove(obj2);
                            GameEntities.Remove(obj1);

                        }
                        else {
                            obj1.Hit();
                            GameEntities.Remove(obj2);
                        }
                        
                        Shooting = false;
                    }
                }
            }
            if (GameEntities[i] is Player)
            {
                Player player = (Player)GameEntities[i];
                if (Inputdirection.GetVector() != null)
                {
                    player.Direction = Inputdirection.GetVector();
                }
                if (player.Direction != null && Inputdirection.GetVector() != null)
                {
                    player.IsShooting = this.Shoot;
                    player.Update();
                }

            }
            else
            {
                GameEntities[i].Update();
            }
        }
       
    }

    private void HandlerShoot(object sender, EventArgs e) {
        Laser.MediaPlayer.Play();
        Player playerSender = (Player)sender;
        playerSender.IsShooting = false;
        Shoot = false;
        Shooting = true;
        Bullet bullet = new Bullet(this.GameScreen) { Position = new Vector2D(playerSender.Position.X, playerSender.Position.Y - playerSender.RectHeight), Speed = 5, id = -1230 };
        bullet.CollisionDetected += BulletCanvasCollisionHandler;
        GameEntities.Add(bullet);
    }

    public void BulletCanvasCollisionHandler(object sender, EventArgs e) {
        Shooting = false;
        GameEntities.Remove((Bullet)sender);
    }

    private void PlayerCollsionHandler(object sender, EventArgs e) {
        Player playerSender = (Player)sender;
        playerSender.Direction = Directions.NULL.GetVector();
    }

    private void GenerateEnemies() {
        int distance = 0;
        int DistanceY = 0;
        for (int i = 0; i < 44; i++)
        {
            distance += 40;
            if ((i) % 11 == 0)
            {
                distance = 40;
                DistanceY += 50;
            }
            Enemy enemy = new Enemy(GameScreen) { Position = new Vector2D(distance, DistanceY), Speed = 0, Direction = Directions.RIGHT.GetVector(), id = distance };
            enemy.CollisionDetected += MoveDownMatrix;
            GameEntities.Add(enemy);
        }
    }

    private void GenerateDefences() {
        int distance = 0;

        for (int i = 0; i < 4; i++) {
            if (i == 0)
            {
                distance += 50;
            }
            else {
                distance += (70 + 40);
            }
            Defence defence = new Defence(GameScreen) { Position = new Vector2D(distance, (int)GameScreen.Height - 150) };
            GameEntities.Add(defence);
        }
       
    }

    private void GeneratePlayer() {
        Player player = new Player(GameScreen) { Position = new Vector2D(10, (int)GameScreen.Height - 20), Speed = 10, id = -1 };
        player.Shoot += HandlerShoot;
        player.CollisionDetected += PlayerCollsionHandler;
        GameEntities.Add(player);
    }
}
