using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpaceInvadersMVVM.core;

namespace SpaceInvadersMVVM.GameEntites;

// la idea de esta clase es regresar en cada update un array de los enemigos con sus nuevas propiedades

public class EnemyMarix : ObservableObject
{
    private ObservableCollection<DynamicGameObject> _enemies = new ObservableCollection<DynamicGameObject>();
    private Canvas gameScreen;
    private bool _isColliding;

    public Canvas GameScreen {
        get { return gameScreen; }
        set { SetProperty(ref gameScreen, value, nameof(GameScreen)); }
    }
    public ObservableCollection<DynamicGameObject> Enemies
    {
        get { return _enemies; }
        set { SetProperty(ref _enemies, value, nameof(Enemies)); }
    }
    public EnemyMarix(ObservableCollection<DynamicGameObject>  enemies, Canvas gameScreen) 
    {
        GameScreen = gameScreen;
        foreach (var enemy in enemies)
        {
            Enemy enemy2 = new Enemy(gameScreen) { Speed = enemy.Speed, Asset = enemy.Asset, id = enemy.id, HP = enemy.HP, Position = new Vector2D(enemy.Position.X, enemy.Position.Y + 20), RectHeight = enemy.RectHeight, RectWidth = enemy.RectWidth, Source = enemy.Source, Direction = new Vector2D(enemy.Direction.X * -1, enemy.Direction.Y) };
            Enemies.Add(enemy2);
        }
    }

    public ObservableCollection<DynamicGameObject> Update()
    {
        for (int i = 0; i < Enemies.Count(); i++) {
            Enemies[i].Move();
            if (Enemies[i].CheckCanvasCollision())
            {
                _isColliding = true;
                break;
            }
        }

        if (_isColliding) {
            for (int i = 0; i < Enemies.Count(); i++)
            {
                Enemies[i].Position.Y = Enemies[i].Position.Y + 20;
                Enemies[i].Direction.X = Enemies[i].Direction.X * -1;
            }
        }
        return Enemies;
    }

}
