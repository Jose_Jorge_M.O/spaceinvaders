using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpaceInvadersMVVM.core;

namespace SpaceInvadersMVVM.GameEntites;
public class Player : DynamicGameObject
{
    public event EventHandler Shoot;
    private bool _Shooting;

    public bool IsShooting {  get { return _Shooting; } set { SetProperty(ref _Shooting, value, nameof(IsShooting)); } }
    public Player(Canvas gameScreen) : base(gameScreen)
    {
        this.Source = "ms-appx:///SpaceInvadersMVVM/Assets/spaceInvader/baseshipa.ico";
        this.RectWidth = 20;
        this.RectHeight = 20;
    }

    public virtual void OnShoot()
    {
        Shoot?.Invoke(this, EventArgs.Empty);
    }

    public override void Update()
    {
        
        CheckCanvasCollision();
        Move();
        if (IsShooting) {
            OnShoot();
        }
    }
}
