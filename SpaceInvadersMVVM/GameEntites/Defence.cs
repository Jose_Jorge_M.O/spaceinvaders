using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpaceInvadersMVVM.core;

namespace SpaceInvadersMVVM.GameEntites;
internal class Defence : GameObject
{

    public Defence(Canvas gameScreen) : base(gameScreen)
    {
        this.Source = "ms-appx:///SpaceInvadersMVVM/Assets/spaceInvader/base-shelter.png";
        this.RectWidth = 70;
        this.RectHeight = 70;
        this.HP = 4; // 3 - 2- 1
    }

    public override void Update()
    {
        base.Update();
    }

    public override void Hit()
    {
        if (HP > 0) {
            this.Source = $"ms-appx:///SpaceInvadersMVVM/Assets/spaceInvader/base-shelter-{HP}.png";
            this.HP -= 1 ;
        }
        else { 
            GC.Collect();
        }
    } 
}
