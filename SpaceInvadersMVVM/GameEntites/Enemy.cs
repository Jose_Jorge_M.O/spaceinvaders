using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpaceInvadersMVVM.core;

namespace SpaceInvadersMVVM.GameEntites;
public class Enemy : DynamicGameObject
{
    public Enemy(Canvas gameScreen) : base(gameScreen) {
        this.Source = "ms-appx:///SpaceInvadersMVVM/Assets/spaceInvader/saucer1b.ico";
        this.RectWidth = 20;
        this.RectHeight = 20;
    }

    public override void Update()
    {
        Move();
        CheckCanvasCollision();
    }
}
