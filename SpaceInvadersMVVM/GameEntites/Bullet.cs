using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpaceInvadersMVVM.core;

namespace SpaceInvadersMVVM.GameEntites;
public class Bullet : DynamicGameObject
{

    public Bullet(Canvas gameScreen) : base(gameScreen)
    {
        this.Source = "ms-appx:///SpaceInvadersMVVM/Assets/spaceInvader/ship-projectile.png";
        this.RectWidth = 20;
        this.RectHeight = 30;
        
        Direction = Directions.UP.GetVector();
    }

    public override void Update()
    {
        CheckCanvasCollision();
        Move();
        
    }

}
