using Microsoft.UI.Xaml.Input;
using SpaceInvadersMVVM.ViewModel;
using Windows.Media.Core;
using Windows.Media.Playback;

namespace SpaceInvadersMVVM;

public sealed partial class MainPage : Page
{
    public GameViewModel GameViewModel { get; set; }

    public MainPage()
    {
        this.InitializeComponent();
        GameViewModel = new GameViewModel(miCanvas);
        GameViewModel.Laser = laser;
        GameViewModel.Explosion = explosion;
        DataContext = GameViewModel;
        GameViewModel.Inputdirection = core.Directions.NULL;
        
    }

    private void miCanvas_PointerEntered(object sender, PointerRoutedEventArgs e)
    {
        miCanvas.Focus(FocusState);
    }

    private void miCanvas_KeyDown(object sender, KeyRoutedEventArgs e)
    {
        if (e.Key == Windows.System.VirtualKey.A || e.Key == Windows.System.VirtualKey.Left) {
            GameViewModel.Inputdirection = core.Directions.LEFT;
        }
        if (e.Key == Windows.System.VirtualKey.D || e.Key == Windows.System.VirtualKey.Right)
        {
            GameViewModel.Inputdirection = core.Directions.RIGHT;
        }
        if (e.Key == Windows.System.VirtualKey.Up || e.Key == Windows.System.VirtualKey.Space) {
            GameViewModel.InpuShoot();
        }
    }

    private void miCanvas_KeyUp(object sender, KeyRoutedEventArgs e)
    {
        GameViewModel.Inputdirection = core.Directions.NULL;
    }
}
